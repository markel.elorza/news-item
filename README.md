# FriendlyURLs - MVC Exercise 2

* Create a new project based on **11 DAO User**.
* Add **Friendly URLs.** 
* Filtering:
    * Filter JSP Files
    * Use a **new filter class** to manage permissions for user actions.
        * Anyone can create a User.
        * Only logged users can **view, list, edit, create** or **delete** users.
        * Moreover, users can only **edit** or **delete** their own users.
* User Controller Actions:
    * ```GET /user``` & ```GET /user/list``` -> Show ```user_list.jsp```
    * ```GET /user/create``` -> Show Empty ```user_form.jsp```.
    * ```POST /user/create``` > Store user in DB and redirect to ```/user/{id}```
    * ```GET /user/{id}``` Show ```user.jsp``` with user data.
    * ```GET /user/{id}/edit``` -> Show filled ```user_form.jsp```
    * ```POST /user/{id}/edit``` -> Update user in DB and redirect to ```/user/{id}```
    * ```GET /user/{id}/delete``` Delete user from DB and redirect to ```/user/list```
* You may need to modify the servlet helper class.

