/*
 * This is a Java Bean.
 * These are the unique characteristics they must have:
 *     -Default, no-argumented constructor.
 *     -It should be serializable and implement Serializable interface.
 *     -It may have a number of properties which can be read or written.
 *     -"getters" and "setters" for those properties.
 */
package domain.newsitem.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import domain.user.model.User;

/**
 * @author beppo
 *
 */
public class NewsItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7688928851149870938L;
	
	private int newsItemId = 0;
	private String title = null;
	private String body = null;
	private Date date = null;
	private Locale lang = null;
	private User author = null;
	

	public NewsItem() {}
	
	public int getNewsItemId() {
		return newsItemId;
	}

	public void setNewsItemId(int newsItemId) {
		this.newsItemId = newsItemId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	public Locale getLang() {
		return lang;
	}

	public void setLang(Locale lang) {
		this.lang = lang;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}	
	
	public String getDateString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");  
        return dateFormat.format(this.date);  
	}

}
