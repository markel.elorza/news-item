/**
 * 
 */
package domain.newsitem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


import config.MySQLConfig;
import domain.newsitem.model.NewsItem;
import domain.user.dao.UserFacade;
import domain.user.model.User;

/**
 * @author beppo
 *
 */
public class DaoNewsItemMySQL implements DaoNewsItem {

	private MySQLConfig mysqlConfig;
	
	public DaoNewsItemMySQL() {
		mysqlConfig = MySQLConfig.getInstance();	
	}
	
	@Override
	public void createNewsItem(NewsItem newsItem) {
		String sqlQuery = "INSERT INTO news_item (title, body, lang, authorId) " + 
				"VALUES (?, ?, ?, ?); ";
		Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    try{
	        stm = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
	        stm.setString(1, newsItem.getTitle());
	        stm.setString(2, newsItem.getBody());
	        stm.setString(3, newsItem.getLang().getLanguage());
	        stm.setInt(4, newsItem.getAuthor().getUserId());
	      
	        if(stm.executeUpdate()>0){
	            // Get the ID
	            try (ResultSet generatedKeys = stm.getGeneratedKeys()) {
	              if (generatedKeys.next()) {
	                  newsItem.setNewsItemId(generatedKeys.getInt(1));
	              }
	              else {
	                  throw new SQLException("Creating user news Item, no ID obtained.");
	              }
	            }
	          }else {
	            throw new SQLException("Creating news Item failed, no rows affected.");
	          }
	      }catch(SQLException e){
	        e.printStackTrace();
	        System.out.println("Error DaoNewsItemMySQL createNewsItem ");
	      }
	}
	
	@Override
	public NewsItem loadNewsItem(int newsItemId) {
		String sqlQuery = "SELECT * FROM news_item WHERE newsItemId=?";
	    NewsItem newsItem = null;
	    Connection connection = mysqlConfig.connect();
	    PreparedStatement stm = null;
	    try{
	      stm = connection.prepareStatement(sqlQuery);
	      stm.setInt(1, newsItemId);
	      System.out.println(stm);
	      ResultSet rs = stm.executeQuery();
	      if(rs.next()){
	    	newsItem = new NewsItem();
	    	fillNewsItem(newsItem, rs);
	      }
	    }catch(SQLException e){
	      e.printStackTrace();
	      System.out.println("Error DaoNewsItemMySQL loadNewsItem");
	    }
	    mysqlConfig.disconnect(connection, stm);
	    return newsItem;
	}

	@Override
	public ArrayList<NewsItem> loadNewsItems(Locale locale) {
		String sqlQuery = "SELECT * FROM news_item WHERE lang =?";
	    Connection connection = mysqlConfig.connect();
	    NewsItem newsItem = null;
	    PreparedStatement stm = null;
	    ArrayList<NewsItem> newsItems = new ArrayList<>();
	    try {
	    	stm = connection.prepareStatement(sqlQuery);
	    	stm.setString(1, locale.getLanguage());
	    	System.out.println(sqlQuery);
	    	ResultSet rs = stm.executeQuery();
	    	while (rs.next()) {
	    		newsItem = new NewsItem();
	    		fillNewsItem(newsItem, rs);
	    		newsItems.add(newsItem);
	    	}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error DaoUserMysql loadUsers");
		}
		return newsItems;
	}

	@Override
	public NewsItem editNewsItem(NewsItem NewsItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteNewsItem(int NewsItemId) {
		// TODO Auto-generated method stub

	}

	private void fillNewsItem(NewsItem newsItem, ResultSet rs) {
		try {
			newsItem.setNewsItemId(rs.getInt("newsItemId"));
			newsItem.setTitle(rs.getString("title"));
			newsItem.setBody(rs.getString("body"));
			Date date = null;
			date = rs.getDate("date");
			newsItem.setDate(date);
			if(rs.getString("lang") != null) newsItem.setLang(new Locale(rs.getString("lang")));
			else newsItem.setLang(null);
			User user = null;
			UserFacade userfc = new UserFacade();
			user = userfc.loadUser(rs.getInt("authorId"));
			
			newsItem.setAuthor(user);
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error DaoNewsItemMysql fillNewsItem ");
		}
	}

}
