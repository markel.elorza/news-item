package domain.newsitem.dao;

import java.util.ArrayList;
import java.util.Locale;

import domain.newsitem.model.NewsItem;

public class NewsItemFacade {
	DaoNewsItem daoNewsItem = null;
	
	public NewsItemFacade() {
		daoNewsItem = new DaoNewsItemMySQL();
	}

	
	public NewsItem loadNewsItem(int newsItemId) {
		return daoNewsItem.loadNewsItem(newsItemId);
	}
	
	public ArrayList<NewsItem> loadNewsItems(Locale locale) {
		return daoNewsItem.loadNewsItems(locale);
	}
	
	public void createNewsItem(NewsItem newsItem) {
		daoNewsItem.createNewsItem(newsItem);
	}
	
	public NewsItem editNewsItem(NewsItem newsItem) {
		return daoNewsItem.editNewsItem(newsItem);
	}
	
	public void deleteNewsItem(int newsItemId) {
		daoNewsItem.deleteNewsItem(newsItemId);
	}
}
