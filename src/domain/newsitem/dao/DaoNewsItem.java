/**
 * 
 */
package domain.newsitem.dao;

import java.util.ArrayList;
import java.util.Locale;

import domain.newsitem.model.NewsItem;

/**
 * @author beppo
 *
 */
public interface DaoNewsItem {
//	public void insertNewsItem(NewsItem newsItem);
	public void createNewsItem(NewsItem newsItem);
	public NewsItem loadNewsItem(int newsItemId);
	public ArrayList<NewsItem> loadNewsItems(Locale locale);
	public NewsItem editNewsItem(NewsItem newsItem);
	public void deleteNewsItem(int newsItemId);
}
