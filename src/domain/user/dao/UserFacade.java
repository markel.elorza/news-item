package domain.user.dao;

import java.util.ArrayList;

import domain.user.model.User;

public class UserFacade {
	DaoUser daoUser = null;
	
	public UserFacade(){
		daoUser = new DaoUserMySQL();
	}
	public User loadUser(String username, String password){
		return daoUser.loadUser(username,password);
	}
	public User loadUser(int userId){
		return daoUser.loadUser(userId);
	}
	public ArrayList<User> loadUsers(){
		return daoUser.loadUsers();
	}
	
	public int createUser(User user) {
		return daoUser.createUser(user);
	}
	public User editUser(User user) {
		return daoUser.editUser(user);		
	}
	public void deleteUser(int userId) {
		daoUser.deleteUser(userId);
		
	}

}
