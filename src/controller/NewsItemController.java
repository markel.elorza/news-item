package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import domain.newsitem.dao.NewsItemFacade;
import domain.newsitem.model.NewsItem;
import domain.user.model.User;
import helper.ControllerHelper;

/**
 * Servlet implementation class NewsItem
 */
@WebServlet("/news/*")
public class NewsItemController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewsItemController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControllerHelper controllerHelper = new ControllerHelper(request);
		int newsItemId = controllerHelper.getId();
	    String action = controllerHelper.getAction();
	    
	    switch (action) {
		case "list":
			listNewsItems(request, response);
			break;
		case "create":
			createNewsItemForm(request, response);
			break;
		default:
			break;
		}
	    
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControllerHelper controllerHelper = new ControllerHelper(request);
		int newsItemId = controllerHelper.getId();
	    String action = controllerHelper.getAction();
	    
	    switch (action) {
		case "create":
			createNewsItem(request, response);
			break;
		default:
			break;
		}
	}


	private void listNewsItems(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ArrayList<NewsItem> newsItem = null;
		NewsItemFacade nf = new NewsItemFacade();
		Locale lang = getLocale(request, request.getSession());
		newsItem = nf.loadNewsItems(lang);
		
		request.setAttribute("newsItems", newsItem);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/pages/news-item/news_list.jsp");
		dispatcher.forward(request, response);
	}
	
	private void createNewsItemForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("currentPage", "form");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/pages/news-item/news_form.jsp");
		dispatcher.forward(request, response);
	}
	
	private void createNewsItem(HttpServletRequest request, HttpServletResponse response) throws IOException {
		NewsItemFacade fc = new NewsItemFacade();
		Locale lang = null;
		User user = null;
		HttpSession session = request.getSession(false);
		
		NewsItem newsItem = new NewsItem();
		fillNewsItem(newsItem, request);
		
		lang = getLocale(request, session);
		newsItem.setLang(lang);
		user = (User) session.getAttribute("user");
		newsItem.setAuthor(user);
		
		fc.createNewsItem(newsItem);
		response.sendRedirect(request.getContextPath() + "/news/");
	}

	private void fillNewsItem(NewsItem newsItem, HttpServletRequest request) {
		newsItem.setTitle(request.getParameter("title"));
		newsItem.setBody(request.getParameter("body"));
	}
	
	private Locale getLocale(HttpServletRequest request, HttpSession session) {
		Locale lang = null;
		
		lang = (Locale) Config.get(session, javax.servlet.jsp.jstl.core.Config.FMT_LOCALE);
		if (lang == null) lang = request.getLocale();
		if (lang == null) lang = new Locale("en");
		
		return lang;
	}

}
