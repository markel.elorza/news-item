package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import helper.ControllerHelper;

@WebServlet("/friendly/*")
public class FriendlyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	public FriendlyController() {
	  super();
  }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  // Friendly URL Controller Helper will split the url and return the element ID and the action String.
	  ControllerHelper controllerHelper = new ControllerHelper(request);
	  int id = controllerHelper.getId();
	  String action = controllerHelper.getAction();
	  
	  request.setAttribute("id", id);
    request.setAttribute("action", action);
    
    RequestDispatcher dispatcher = getServletContext()
            .getRequestDispatcher("/page.jsp");
    dispatcher.forward(request, response);
	  
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
