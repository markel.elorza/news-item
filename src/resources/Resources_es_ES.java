package resources;
import java.util.ListResourceBundle;


public class Resources_es_ES extends ListResourceBundle{
  private static final Object[][] contents = {
	  {"string.lang.en", "Inglés"},
      {"string.lang.es", "Castellano"},
      {"string.lang.eu", "Euskera"},
      {"string.page.title", "User CRUD exercise"},
      {"string.menu.list", "Listar Usuarios"},
      {"string.menu.create","Crear Usuario"},
      {"string.login","Login"},
      {"string.username","Nombre de usuario"},
      {"string.password","Contraseña"},
      {"string.user.id","ID"},
      {"string.user.username","Usuario"},
      {"string.user.email","Correo"},
      {"string.user.firstname","Nombre"},
      {"string.user.secondname","Apellido"},
      {"string.user.password","Contraseña"},
      {"string.action.edit","Editar"},
      {"string.action.delete","Borrar"},
      {"string.hi", "Bienvenido"},
      {"string.logged", "Estas conectado!"},
      {"string.logout", "Desconectarte"},
      {"string.title.list", "Lista de usuarios"},
      {"string.title.form", "Formulario de usuarios"},
      {"string.title.create", "Nuevo usuario"},
      {"string.title.edit", "Editar usuario"},
      {"string.title.details", "Detales de usuario"},
      {"string.title.error", "Pagina Error"},
      {"string.menu.home", "Inicio"},
      {"string.button.create", "Crear"},
      {"string.button.edit", "Editar"}
  };
  
  @Override
  protected Object[][] getContents() {
    return contents;
  }

}
