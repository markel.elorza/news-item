package resources;
import java.util.ListResourceBundle;


public class Resources_eu extends ListResourceBundle{
  private static final Object[][] contents = {
		  {"string.lang.en", "Ingelesa"},
	      {"string.lang.es", "Erdera"},
	      {"string.lang.eu", "Euskara"},
	      {"string.page.title", "User CRUD exercise"},
	      {"string.menu.list", "Erabiltzileak listatu"},
	      {"string.menu.create","Erabiltzailea sortu"},
	      {"string.login","Logeatu"},
	      {"string.username","Erabiltzaile izena"},
	      {"string.password","Pasahitza"},
	      {"string.user.id","ID"},
	      {"string.user.username","Erabiltzailea"},
	      {"string.user.email","Posta elektronikoa"},
	      {"string.user.firstname","Izena"},
	      {"string.user.secondname","Abizena"},
	      {"string.user.password","Pasahitza"},
	      {"string.action.edit","Editatu"},
	      {"string.action.delete","Ezabatu"},
	      {"string.hi", "Ongi etorri"},
	      {"string.logged", "Konektatuta zaude!"},
	      {"string.logout", "Deskonektatu"},
	      {"string.title.list", "Erabiltzaileen lista"},
	      {"string.title.form", "Erabiltzaile Formularioa"},
	      {"string.title.create", "Erbiltzaile Berria"},
	      {"string.title.edit", "Erabiltzailea Editatu"},
	      {"string.title.details", "Erabiltzaile detaileak"},
	      {"string.title.error", "Errore pagina"},
	      {"string.menu.home", "Hasiera"},
	      {"string.button.create", "Sortu"},
	      {"string.button.edit", "Editatu"}
  };
  
  @Override
  protected Object[][] getContents() {
    return contents;
  }

}
