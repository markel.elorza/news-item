<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fmt:bundle basename="resources.Resources">
		<header>
			<h1><fmt:message key="string.page.title"/></h1>
			<nav>
				<div class="action-menu">
					<a href="${pageContext.request.contextPath}/" class="${requestScope.currentPage=='home' ? 'active':''}" ><fmt:message key="string.menu.home"/></a>
					<c:if test="${not empty sessionScope.user}">  
						<a href="${pageContext.request.contextPath}/user/" class="${requestScope.currentPage=='list' ? 'active':''}" ><fmt:message key="string.menu.list"/></a>
						<a href="${pageContext.request.contextPath}/news/" class="${requestScope.currentPage=='news-list' ? 'active':''}" ><fmt:message key="string.menu.news.item.list"/></a>
						<a href="${pageContext.request.contextPath}/news/create" class="${requestScope.currentPage=='news-create' ? 'active':''}" ><fmt:message key="string.menu.news.item.create"/></a>
					</c:if>
					<a href="${pageContext.request.contextPath}/user/create" class="${requestScope.currentPage=='form' ? 'active':''}" ><fmt:message key="string.menu.create"/></a>
				</div>
				<div class="lang-menu">
					<a href="${pageContext.request.contextPath}/LocaleController?language=en&country=UK"
					class="${fn:startsWith(sessionScope['javax.servlet.jsp.jstl.fmt.locale.session'],'en') ? 'active' : '' }"><fmt:message key="string.lang.en"/></a>
					<a href="${pageContext.request.contextPath}/LocaleController?language=es&country=ES"
					class="${fn:startsWith(sessionScope['javax.servlet.jsp.jstl.fmt.locale.session'],'es') ? 'active' : '' }"><fmt:message key="string.lang.es"/></a>
					<a href="${pageContext.request.contextPath}/LocaleController?language=eu&country=ES"
					class="${fn:startsWith(sessionScope['javax.servlet.jsp.jstl.fmt.locale.session'],'eu') ? 'active' : '' }"><fmt:message key="string.lang.eu"/></a>
				</div>
				
			</nav>
		</header>
</fmt:bundle>		