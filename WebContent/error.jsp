<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="string.title.error"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/details.css">
</head>
<body>
<jsp:include page="/templates/header.jsp"/>
<fmt:bundle basename="resources.Resources">
<div class="content">
	<h1>JSP Error</h1>
	<p>You were trying to access jsp files directly. It is not allowed.</p>
</div>
</fmt:bundle>
<jsp:include page="/templates/footer.jsp"/>