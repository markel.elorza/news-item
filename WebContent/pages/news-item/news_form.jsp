<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="basePath" scope = "page" value="${pageContext.request.contextPath}"/>

<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:choose>
	<c:when test="${not empty requestScope.newsItem}">
		<title><fmt:message key="string.title.edit"/></title>
	</c:when>
	<c:otherwise>
		<title><fmt:message key="string.title.create"/></title>
	</c:otherwise>
</c:choose>
<link rel="stylesheet" type="text/css" href="${basePath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${basePath}/css/form.css">
</head>
<body>
<jsp:include page="../../templates/header.jsp"/>
<div class="content">
	<h2><fmt:message key="string.title.form"/></h2>
	<div class="form-container">
		<form action="${not empty requestScope.newsItem ? 'edit' : 'create'}" method="POST">
			<c:choose>
				<c:when test="${not empty requestScope.user}">
					<h2 class="form-title"><fmt:message key="string.title.edit"/></h2>
				</c:when>
				<c:otherwise>
					<h2 class="form-title"><fmt:message key="string.title.create"/></h2>
				</c:otherwise>
			</c:choose>
			
			<label for="title"><fmt:message key="string.newsItem.title"/></label>
			<input type="text" name="title" value="${not empty requestScope.newsItem ? requestScope.newsItem.title : ''}"/>
			<label for="body"><fmt:message key="string.newsItem.body"/></label>
			<textarea name="body">${not empty requestScope.newsItem ? requestScope.newsItem.body : ''}</textarea>
			<c:choose>
				<c:when test="${not empty requestScope.newsItem}">
					<button type="submit" ><fmt:message key="string.button.edit"/></button>
				</c:when>
				<c:otherwise>
					<button type="submit" ><fmt:message key="string.button.create"/></button>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</div>
</fmt:bundle>
<jsp:include page="../../templates/footer.jsp"/>