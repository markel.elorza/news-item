<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><fmt:message key="string.title.list"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/news-list.css">
</head>
<body>
<jsp:include page="../../templates/header.jsp"/>
<div class="content">
	<!-- <h2><fmt:message key="string.title.list"/></h2> -->
	<div class="container">
		<c:forEach items="${requestScope.newsItems}" var="newsItem" >
			<article class="news-item">
				<div class="news-header">
			    	<h2>${newsItem.title}</h2>
			  	</div>
			  	<div class="news-content">
			  		<p>${newsItem.body}</p>
			  	</div>
			  	<div class="news-footer">
			  		<span class="news-user">User: <a href="">${newsItem.author.username}</a></span>
			  		<span class="news-lang">${newsItem.lang}</span>
			  		<span class="news-date">${newsItem.dateString}</span>
			  	</div>
			</article>
		</c:forEach>
	</div>
</div>
</fmt:bundle>
<jsp:include page="../../templates/footer.jsp"/>