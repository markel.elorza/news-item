<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:set var="username" scope="page" value="${'Alain'}"/>
<title><fmt:message key="string.title.list"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/list.css">
</head>
<body>
<jsp:include page="../../templates/header.jsp"/>
<div class="content">
	<h2><fmt:message key="string.title.list"/></h2>
	<table>
		<thead>
			<th><fmt:message key="string.user.id"/></th><th><fmt:message key="string.user.username"/></th><th><fmt:message key="string.user.email"/></th><th><fmt:message key="string.action.edit"/></th><th><fmt:message key="string.action.delete"/></th>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.userList}" var="user" >
				<tr>
					<td><a href="${pageContext.request.contextPath}/user/${user.userId}">${user.userId}</a></td>
					<td>${user.username}</td>
					<td>${user.email}</td>
					<c:choose>
					<c:when test="${sessionScope.user.userId eq user.userId}">
				    	<td><a href="${pageContext.request.contextPath}/user/${user.userId}/edit">Edit</a></td>
						<td><a href="${pageContext.request.contextPath}/user/${user.userId}/delete">Delete</a></td>
				  	</c:when>
				  	<c:otherwise>
				    	<td>-</td>
						<td>-</td>
				  	</c:otherwise>
				  	</c:choose>
				</tr>
		 	</c:forEach>
		</tbody>
	</table>
</div>
</fmt:bundle>
<jsp:include page="../../templates/footer.jsp"/>