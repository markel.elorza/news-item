<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:bundle basename="resources.Resources">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:choose>
	<c:when test="${not empty requestScope.user}">
		<title><fmt:message key="string.title.edit"/></title>
	</c:when>
	<c:otherwise>
		<title><fmt:message key="string.title.create"/></title>
	</c:otherwise>
</c:choose>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/form.css">
</head>
<body>
<jsp:include page="../../templates/header.jsp"/>
<div class="content">
	<h2><fmt:message key="string.title.form"/></h2>
	<div class="form-container">
		<form action="${not empty requestScope.user ? 'edit' : 'create'}" method="POST">
			<c:choose>
				<c:when test="${not empty requestScope.user}">
					<h2 class="form-title"><fmt:message key="string.title.edit"/></h2>
				</c:when>
				<c:otherwise>
					<h2 class="form-title"><fmt:message key="string.title.create"/></h2>
				</c:otherwise>
			</c:choose>
			
			<label for="name"><fmt:message key="string.user.firstname"/></label>
			<input type="text" name="firstName" value="${not empty requestScope.user ? requestScope.user.firstName : ''}"/>
			<label for="secondName"><fmt:message key="string.user.secondname"/></label>
			<input type="text" name="secondName" value="${not empty requestScope.user ? requestScope.user.secondName : ''}"/>
			<label for="email"><fmt:message key="string.user.email"/></label>
			<input type="email" name="email" value="${not empty requestScope.user ? requestScope.user.email : ''}"/>
			<label for="username"><fmt:message key="string.user.username"/></label>
			<input type="text" name="username" value="${not empty requestScope.user ? requestScope.user.username : ''}"/>
			<label for="username"><fmt:message key="string.user.password"/></label>
			<input type="password" name="password"/>
			<input type="hidden" name="userId" value="${not empty requestScope.user ? requestScope.user.userId : ''}" />
			<c:choose>
				<c:when test="${not empty requestScope.user}">
					<button type="submit" ><fmt:message key="string.button.edit"/></button>
				</c:when>
				<c:otherwise>
					<button type="submit" ><fmt:message key="string.button.create"/></button>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</div>
</fmt:bundle>
<jsp:include page="../../templates/footer.jsp"/>